#!/usr/bin/env bash
set -euo pipefail

prepare-manifest() {
  echo "$MANIFEST_JSON" | yq eval -P > manifest.yml
}

wait-for-resource-ready() {
  if [[ -z $RESOURCE_READY_TEMPLATE ]]; then
    echo "No resource-ready-template given - skipping wait phase"
    exit 0;
  fi

  sleeptime=2
  if [[ $RESOURCE_READY_TIMEOUT -gt 0 ]]; then
    retries=$((RESOURCE_READY_TIMEOUT/sleeptime))
  else
    retries=-1
  fi

  resource_type=$(yq eval '.kind' manifest.yml)
  resource_name=$(yq eval '.metadata.name' manifest.yml)
  resource_namespace=$(yq eval '.metadata.namespace // "default"' manifest.yml)

  while : ; do
    local status
    status="$(kubectl get "$resource_type" --request-timeout="${KUBECTL_TIMEOUT}s" -n "$resource_namespace" "$resource_name" \
      -o go-template="$RESOURCE_READY_TEMPLATE")"

    if [[ $status == "$RESOURCE_READY_VALUE" ]]; then
      echo "Status of $resource_type resource '$resource_name' has desired value '$status' - ready"
      break # done
    fi

    if [[ $retries == 0 ]]; then
      break # retries exhausted
    fi

    echo "Status of $resource_type resource '$resource_name' has value '$status', waiting for '$RESOURCE_READY_VALUE'. Tries left: $([[ $retries -gt 0 ]] && echo $retries || echo '∞')"
    sleep $sleeptime
    if [[ $retries -gt 0 ]]; then
      ((retries--))
    fi
  done

  if [[ $retries == 0 ]]; then
    echo "Error: resource not ready, retries exhausted"
    exit 1
  fi
}

# Call the requested function and pass the arguments as-is
"$@"
